import React from 'react'
import moment from 'moment'




class MovieRow extends React.Component {

  viewMovie(){
    const url = "https://www.themoviedb.org/movie/" + this.props.movie.id
    window.location.href = url
    

  }
   render(){ 
     
    let momentObj = moment(this.props.movie.release_date, 'YYYY-MM-DD')
    let showDate = moment(momentObj).format('DD-MM-YYYY')
    
      return  <div className="ml-4 mt-4 mr-4 card h-25 shadow-lg" key={this.props.movie.id}>
        
               
      <div className="row">
      <div className="col">
      <img id="posterPath" type="button" className="ml-4 mt-4 mb-4 card-img-top w-50" alt="poster" src={this.props.movie.poster_src} onClick={this.viewMovie.bind(this)}/>
      </div>
     
      <div className="col">
      <div className="h5 ml-4 mt-4 text-center font-weight-bold">{this.props.movie.title}</div>
      <div className="ml-4 mr-4 mt-4"> {this.props.movie.overview}</div>
      <div className="ml-4 mr-4 mt-4 mb-4 font-weight-bold">Date de sortie: {showDate}</div>
      
      <button className="btn btn-primary ml-4 mr-4 mt-4 mb-4 font-weight-bold" type="button" onClick={this.viewMovie.bind(this)}>Voir le film</button>
      
      </div>  
  
</div>

              </div>
              
   }
}

export default MovieRow


  
  