import React, { Component} from 'react';
import './App.css';
import $ from 'jquery'; 
import 'animate.css';
import MovieRow from './MovieRow.js'
import Pagination from "react-js-pagination";
import axios from 'axios'




class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      activePage: 1
    }
    this.performSearch("woman")
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({activePage: pageNumber});
    this.performSearch("woman")
  }

performSearch(searchTerme){

  console.log("Perform search using moviedb")
  const urlString = `https://api.themoviedb.org/3/search/movie?api_key=2f6906b27972d6d4275b9163c1b79339&language=fr-US&query=hjhj&include_adult=false&query=${searchTerme}&page=${this.state.activePage}`;
  $.ajax({
    url: urlString,
    success: (searchResults) => {
      //console.log("Fetched data successfully")
      const results = searchResults.results
     

      let movieRows = []

      results.forEach((movie) => {
        movie.poster_src = 'https://image.tmdb.org/t/p/w500' + movie.poster_path;
        const movieRow = <MovieRow key={movie.id} movie={movie}/>
        movieRows.push(movieRow)
      }) 

      this.setState({rows: movieRows})
    },

    error: (xhr, status, err) => { 
    //console.error ("Failed to fetch data ")
    }

  })

 
}

searchChangeHandler(event) {
  console.log(event.target.value)
  const boundObject = this
  const searchTerme = event.target.value
  boundObject.performSearch(searchTerme)
}



  render () {
    return (

      
      <div>
       
            <div className="container">
              <div className="row justify-content-center">
              <div><img className="col justify-content-center animate__animated animate__pulse" src="/cinema.png" alt="Allo cine"/></div>
                
              <div className="col h1 text-primary mt-5 text-center">Allocine</div>
             
              </div>
            </div>
            <div className="row">
            <input className="form-control form-control-lg mt-4 mr-4 ml-4 shadow" type="text" placeholder="Recherche" onChange={this.searchChangeHandler.bind(this)}></input>
            <div className="row mt-4 ml-4">

        <Pagination
          
          itemClass="page-item"
          linkClass="page-link"
          activePage={this.state.activePage}
          itemsCountPerPage={5}
          totalItemsCount={50}
          pageRangeDisplayed={5}
          onChange={this.handlePageChange.bind(this)}/>
      </div>
          </div>
 
        {this.state.rows}

    </div>
    );
  }
}



export default App;



